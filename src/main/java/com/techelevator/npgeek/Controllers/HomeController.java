package com.techelevator.npgeek.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.npgeek.Models.Park;
import com.techelevator.npgeek.Models.ParkDAO;
import com.techelevator.npgeek.Models.Survey;
import com.techelevator.npgeek.Models.SurveyDAO;
import com.techelevator.npgeek.Models.Weather;
import com.techelevator.npgeek.Models.WeatherDAO;
import com.techelevator.npgeek.Models.jdbcParkDAO;

@Controller
@SessionAttributes("tempScale")
public class HomeController {
	@Autowired
	private ParkDAO parkDao;
	@Autowired
	private WeatherDAO weatherDao;
	
	@Autowired
	private SurveyDAO surveyDao;
	
	@RequestMapping("/")
		public String displayHomePage(HttpServletRequest request , ModelMap model){
			List <Park> parkList= parkDao.getAllParks();
			request.setAttribute("parkList", parkList);
			if (!model.containsAttribute("tempScale")){
				Boolean isFahrenheit = true;
				model.put("tempScale", isFahrenheit);
			}
			return "HomePage";
		}
	@RequestMapping("/ParkDetailView")
	public String displayDetailView(HttpServletRequest request, @RequestParam String code, ModelMap model){
		Park park = parkDao.getParkFromCode(code);
		List<Weather> weatherList = weatherDao.getWeatherForecast(code);
		request.setAttribute("park", park);
		request.setAttribute("weatherList", weatherList);
		try {
			request.getParameter("tempscale").equals("F");
		} catch (Exception e) {
			return "ParkDetailView";
		}
		if (request.getParameter("tempscale").equals("F")){
			Boolean isFahrenheit = true;
			model.put("tempScale", isFahrenheit);
		}else if (request.getParameter("tempscale").equals("C")){
			Boolean isFahrenheit = false;
			model.put("tempScale", isFahrenheit);}
		return "ParkDetailView";
		
	}
	@RequestMapping(path="/SurveyInput", method=RequestMethod.GET)
	public String showSurveyInput(){
		return "SurveyInput";
	}
	@RequestMapping(path="/SurveyInput", method=RequestMethod.POST)
	public String processSurveyInput(Survey post){
		surveyDao.saveSurveyPost(post);
		
		return "redirect:/SurveyResults";
	}
	@RequestMapping("/SurveyResults")
	public String showSurveyResults(HttpServletRequest request){
//		List<Survey> surveyList = surveyDao.getSurveyResults();
		String talliedResults = surveyDao.tallySurveyResults();
//		request.setAttribute("surveyList", surveyList);
		request.setAttribute("talliedResults", talliedResults);
		return "SurveyResults";
	}
	
}


