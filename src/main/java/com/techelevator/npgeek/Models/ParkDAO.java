package com.techelevator.npgeek.Models;

import java.util.List;

public interface ParkDAO {
	public List<Park> getAllParks();

	public Park getParkFromCode(String code);
}
