package com.techelevator.npgeek.Models;

import java.util.List;
import java.util.Map;

public interface SurveyDAO {
//	public List<Survey> getSurveyResults();

	public Survey saveSurveyPost(Survey post);

	public String tallySurveyResults();

}
