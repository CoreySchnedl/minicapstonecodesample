package com.techelevator.npgeek.Models;

import java.util.List;

public interface WeatherDAO{
	public List<Weather> getWeatherForecast(String code);
}
