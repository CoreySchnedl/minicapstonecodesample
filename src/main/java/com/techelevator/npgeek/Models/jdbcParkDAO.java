package com.techelevator.npgeek.Models;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class jdbcParkDAO implements ParkDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public jdbcParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getAllParks() {
		ArrayList<Park> parkList = new ArrayList<Park>();
		String sqlGetAllParks = "SELECT * FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllParks);
		while (results.next()) {
			Park park = mapRowToPark(results);
			parkList.add(park);
		}
		return parkList;
	}

	private Park mapRowToPark(SqlRowSet results) {
		Park park = new Park();
		park.setAcreage(results.getInt("acreage"));
		park.setAnnualvisitorcount(results.getInt("annualvisitorcount"));
		park.setClimate(results.getString("climate"));
		park.setElevationinfeet(results.getInt("elevationinfeet"));
		park.setEntryfee(results.getInt("entryfee"));
		park.setInspirationalquote(results.getString("inspirationalquote"));
		park.setInspirationalquotesource(results.getString("inspirationalquotesource"));
		park.setMilesoftrail(results.getFloat("milesoftrail"));
		park.setNumberofanimalspecies(results.getInt("numberofanimalspecies"));
		park.setNumberofcampsites(results.getInt("numberofcampsites"));
		park.setParkcode(results.getString("parkcode"));
		park.setParkdescription(results.getString("parkdescription"));
		park.setParkname(results.getString("parkname"));
		park.setState(results.getString("state"));
		park.setYearfounded(results.getInt("yearfounded"));
		return park;
	}

	@Override
	public Park getParkFromCode(String code) {
		String sqlGetParkByCode = "SELECT * FROM park WHERE parkcode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetParkByCode, code);
		if (results.next()) {
			Park park = mapRowToPark(results);
			return park;
		} else {
			return null;
		}
	}

}
