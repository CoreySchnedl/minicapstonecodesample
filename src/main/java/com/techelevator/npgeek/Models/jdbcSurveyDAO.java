package com.techelevator.npgeek.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class jdbcSurveyDAO implements SurveyDAO {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public jdbcSurveyDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	//ignore below method -> misunderstood user story
//	@Override
//	public List<Survey> getSurveyResults() {
//		List<Survey> surveyResults = new ArrayList<>();
//		String sqlSelectAllPosts = "SELECT * FROM survey_result";
//		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllPosts);
//		while (results.next()) {
//			Survey post = new Survey();
//			post.setActivitylevel(results.getString("activitylevel"));
//			post.setEmailaddress(results.getString("emailaddress"));
//			post.setParkcode(results.getString("parkcode"));
//			post.setState(results.getString("state"));
//			surveyResults.add(post);
//
//		}
//		return surveyResults;
//	}

	@Override
	public Survey saveSurveyPost(Survey post) {
		String sqlSaveSurveyPost = "INSERT INTO survey_result (parkcode, emailaddress, state, activitylevel) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sqlSaveSurveyPost, post.getParkcode(), post.getEmailaddress(), post.getState(),
				post.getActivitylevel());
		String sqlGetLastPost = "SELECT * from survey_result WHERE surveyid = LASTVAL()";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetLastPost);
		if(results.next())
		{
			Survey postToReturn = new Survey();
			postToReturn.setActivitylevel(results.getString("activitylevel"));
			postToReturn.setParkcode(results.getString("parkcode"));
			postToReturn.setState(results.getString("state"));
			postToReturn.setEmailaddress(results.getString("emailaddress"));
			postToReturn.setSurveyid(results.getInt("surveyid"));
			return postToReturn;
		}
		else {
			return null;
		}
	}

	@Override
	public String tallySurveyResults() {
		String sqlTallySurveyResults = "SELECT parkname FROM park WHERE parkcode=(SELECT parkcode FROM survey_result GROUP BY parkcode ORDER BY count(parkcode) DESC LIMIT 1)";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlTallySurveyResults);
		if (results.next()) {
			return results.getString("parkname");
		}
		return null;
	}
}
