package com.techelevator.npgeek.Models;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class jdbcWeatherDAO implements WeatherDAO {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public jdbcWeatherDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Weather> getWeatherForecast(String code) {
		ArrayList<Weather> weatherList = new ArrayList<Weather>();
		String sqlGetWeatherForecast = "SELECT * FROM weather WHERE parkcode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetWeatherForecast, code);
		while (results.next()) {
			Weather weather = mapRowToWeather(results);
			weatherList.add(weather);
		}

		return weatherList;
	}

	private Weather mapRowToWeather(SqlRowSet results) {
		Weather weather = new Weather();
		weather.setFivedayforecastvalue(results.getInt("fivedayforecastvalue"));
		weather.setForecast(results.getString("forecast"));
		weather.setHigh(results.getInt("high"));
		weather.setLow(results.getInt("low"));
		weather.setParkcode(results.getString("parkcode"));
		return weather;
	}

}
