<%@include file="/WEB-INF/jsp/Header.jsp"%>
<div id="homePageMainBody">

	<c:forEach var="park" items="${parkList}">
		<div class="homePageParkTile">
			<div class="homePageParkTileImgContainer">
				<a href="./ParkDetailView?code=${park.parkcode}"><img
					class="homePageParkTileImg" src="img/parks/${park.parkcode}.jpg" /></a>
			</div>
			<div class="homePageParkTileInfo">
				<div class="homePageParkTileName">
					<a href="./ParkDetailView?code=${park.parkcode}"><span><c:out
								value="${park.parkname}"></c:out> </span> </a>
				</div>
				<div class="homePageParkTileLocation">
					<span><c:out value="${park.state}"></c:out></span>
				</div>
				<div class="homePageParkTileSummary">
					<span><c:out value="${park.parkdescription}"></c:out></span>
				</div>
			</div>
		</div>
	</c:forEach>
</div>
</body>
</html>
