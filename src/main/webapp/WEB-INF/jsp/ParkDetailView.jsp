<%@include file="/WEB-INF/jsp/Header.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="navBarParkSelectDropDown">
	<c:url var="formAction" value="/ParkDetailView" />
	<form method="GET" action="${formAction}">
		<label for="code"></label> <select name="code"
			id="navParkcodeDropdown">
			<option value="${park.parkcode}"><c:out value="---"></c:out></option>
			<option value="CVNP"><c:out
					value="Cuyahoga Valley National Park"></c:out></option>
			<option value="ENP"><c:out value="Everglades National Park"></c:out></option>
			<option value="GCNP"><c:out
					value="Grand Canyon National Park"></c:out></option>
			<option value="GNP"><c:out value="Glacier National Park"></c:out></option>
			<option value="GSMNP"><c:out
					value="Great Smoky Mountains National Park"></c:out></option>
			<option value="GTNP"><c:out
					value="Grand Teton National Park"></c:out></option>
			<option value="MRNP"><c:out
					value="Mount Rainier National Park"></c:out></option>
			<option value="RMNP"><c:out
					value="Rocky Mountain National Park"></c:out></option>
			<option value="YNP"><c:out value="Yellowstone National Park"></c:out></option>
			<option value="YNP2"><c:out value="Yosemite National Park"></c:out></option>
		</select> <Input type="submit" value="<< I would rather look at this park">
	</form>
</div>
<div id="detailPageMainBody">
	<div class="detailPageParkTile">
		<div class="detailPageParkTileImg">
			<img class="detailPageImage" src="img/parks/${park.parkcode}.jpg" />
		</div>
		<div class="detailPageParkTileInfo">
			<div class="detailPageParkTileName">
				<span><c:out value="${park.parkname}"></c:out></span>
			</div>
			<div class="detailPageParkTileLocation">
				<span><c:out value="${park.state}"></c:out></span>
			</div>
			<div class="detailPageParkTileSummary">
				<span><c:out value="${park.parkdescription}"></c:out></span>
			</div>

			<div class="detailPageParkTileQuote">
				<span><c:out value="${park.inspirationalquote}"></c:out></span>
			</div>
			<div class="detailPageParkTileQuoteSource">
				<span><c:out value="Source: ${park.inspirationalquotesource}"></c:out></span>
			</div>
			<div class="">
				<div class="detailPageParkTileAcreage">
					<span><c:out value="Acreage: "></c:out> <fmt:formatNumber
							value="${park.acreage}" groupingUsed="true"></fmt:formatNumber></span>
				</div>
				<div class="detailPageParkTileElevation">
					<span><c:out value="Elevation: "></c:out> <fmt:formatNumber
							value="${park.elevationinfeet}" groupingUsed="true"></fmt:formatNumber></span>
				</div>
				<div class="detailPageParkTileTrails">
					<span><c:out value="Miles of Trail: "></c:out> <fmt:formatNumber
							value="${park.milesoftrail}" groupingUsed="true"></fmt:formatNumber></span>
				</div>
				<div class="detailPageParkTileCampsites">
					<span><c:out value="Number of campsites: "></c:out> <fmt:formatNumber
							value="${park.numberofcampsites}" groupingUsed="true"></fmt:formatNumber></span>
				</div>
				<div class="detailPageParkTileClimate">
					<span><c:out value="Climate: ${park.climate}"></c:out></span>
				</div>
				<div class="detailPageParkTileYearFounded">
					<span><c:out value="Year founded: ${park.yearfounded}"></c:out></span>
				</div>
				<div class="detailPageParkTileVisitors">
					<span><c:out value="Annual visitors: "></c:out> <fmt:formatNumber
							value="${park.annualvisitorcount}" groupingUsed="true"></fmt:formatNumber></span>
				</div>

				<div class="detailPageParkTileEntryFee">
					<span><c:out value="Entry Fee $${park.entryfee}"></c:out></span>
				</div>
				<div class="detailPageParkTileAnimals">
					<span><c:out
							value="Number of animal species: ${park.numberofanimalspecies}"></c:out></span>
				</div>
			</div>
		</div>
		<br>
		<div class="detailPageWeatherContainer">
			<div class="tempScaleSelector">
				<c:url var="formAction" value="/ParkDetailView" />
				<form method="GET" action="${formAction}">
					<c:if test="${tempScale==false}">
						<Input type="radio" name="tempscale" value="F" checked>
						<c:out value="F"></c:out>
					</c:if>
					<c:if test="${tempScale==true}">
						<Input type="radio" name="tempscale" value="C" checked>
						<c:out value="C"></c:out>
					</c:if>
					<br> <input type="hidden" name="code" value="${park.parkcode}">
					<Input id="convertSubmitButton" type="submit"
						value="Convert Temperature Scale">
				</form>
			</div>
			<c:forEach var="weather" items="${weatherList}">
				<div class="weatherContainer">
					<div class="weatherDay">
						<span><c:out value="Day ${weather.fivedayforecastvalue}"></c:out></span>
					</div>
					<div class="weatherHighTemp">
						<c:out value="High Temp "></c:out>
						<c:if test="${tempScale==true}">
							<span class="highTemp"><c:out
									value=" ${weather.high} &degF" escapeXml="false"></c:out></span>
						</c:if>
						<c:if test="${tempScale==false}">
							<span class="highTemp"><c:out
									value=" ${Math.round((weather.high -32) /1.8)} &degC"
									escapeXml="false"></c:out></span>
						</c:if>

					</div>
					<div class="weatherLowTemp">
						<c:out value="Low Temp "></c:out>
						<c:if test="${tempScale==true}">
							<span class="lowTemp"><c:out value="${weather.low} &degF"
									escapeXml="false"></c:out></span>
						</c:if>
						<c:if test="${tempScale==false}">
							<span class="lowTemp"><c:out
									value="${Math.round((weather.low -32) /1.8)} &degC"
									escapeXml="false"></c:out></span>
						</c:if>
					</div>
					<div>
						<c:choose>
							<c:when test="${weather.forecast == 'cloudy'}">
								<img class="weatherImage" src="img/weather/cloudy.png" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${weather.forecast == 'partly cloudy'}">
								<img class="weatherImage" src="img/weather/partlyCloudy.png" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${weather.forecast == 'rain'}">
								<img class="weatherImage" src="img/weather/rain.png" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${weather.forecast == 'snow'}">
								<img class="weatherImage" src="img/weather/snow.png" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${weather.forecast == 'sunny'}">
								<img class="weatherImage" src="img/weather/sunny.png" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${weather.forecast == 'thunderstorms'}">
								<img class="weatherImage" src="img/weather/thunderstorms.png" />
							</c:when>
						</c:choose>
					</div>
					<div class="weatherForecast">
						<span class ="forecastForDay"><c:out value="Forecast ${weather.forecast}"></c:out></span>
					</div>

					<div class="forecastFeedbackAdvice">
						<c:if test="${weather.forecast== 'snow'}">
							<span><c:out value="Pack your snowshoes"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.forecast== 'rain'}">
							<span><c:out
									value="Pack your rain gear and wear waterproof shoes"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.forecast== 'thunderstorms'}">
							<span><c:out
									value="Seek Shelter!! Avoid hiking on exposed ridges!"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.forecast== 'sun'}">
							<span><c:out value="Pack sunblock"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.high >= 75}">
							<span><c:out value="Bring an extra gallon of water"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.high - weather.low >= 20}">
							<span><c:out value="Wear breathable layers"></c:out></span>
							<br>
						</c:if>
						<c:if test="${weather.low <= 20}">
							<span><c:out
									value="It's gonna be cold, probably best to just stay inside today"></c:out></span>
							<br>
						</c:if>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>
</body>
</html>