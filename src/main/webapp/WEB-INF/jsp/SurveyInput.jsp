<%@include file="/WEB-INF/jsp/Header.jsp" %>

	<div class="surveyInputContainer">
		<c:url var="surveyInputFormAction" value="/SurveyInput" />
		<form method="POST" action="${surveyInputFormAction}">
			<div class="surveyInputParkcode">
				<label for="parkcode"><c:out value="Please select a park: "></c:out></label> <select name="parkcode"
					id="surveyParkcodeDropdown">
					<option value="noneselected">---</option>
					<option value="CVNP"><c:out value="Cuyahoga Valley National Park"></c:out></option>
					<option value="ENP"><c:out value="Everglades National Park"></c:out></option>
					<option value="GCNP"><c:out value="Grand Canyon National Park"></c:out></option>
					<option value="GNP"><c:out value="Glacier National Park"></c:out></option>
					<option value="GSMNP"><c:out value="Great Smoky Mountains National Park"></c:out></option>
					<option value="GTNP"><c:out value="Grand Teton National Park"></c:out></option>
					<option value="MRNP"><c:out value="Mount Rainier National Park"></c:out></option>
					<option value="RMNP"><c:out value="Rocky Mountain National Park"></c:out></option>
					<option value="YNP"><c:out value="Yellowstone National Park"></c:out></option>
					<option value="YNP2"><c:out value="Yosemite National Park"></c:out></option>
				</select>
			</div>
			<div class="surveyInputEmailaddress">
				<label for="emailaddress"><c:out value="Please enter email address: "></c:out></label>
				<input type="text" name="emailaddress" />
			</div>
			<div class="surveyInputState"> 
				<label for="state"><c:out value="Please select your state: "></c:out></label> <select
					name="state" id="surveyStateDropdown">
					<option value="noneselected">---</option>
					<option value="Alabama"><c:out value="Alabama"></c:out></option>
					<option value="Alaska"><c:out value="Alaska"></c:out></option>
					<option value="Arizona"><c:out value="Arizona"></c:out></option>
					<option value="Arkansas"><c:out value="Arkansas"></c:out></option>
					<option value="California"><c:out value="California"></c:out></option>
					<option value="Colorado"><c:out value="Colorado"></c:out></option>
					<option value="Connecticut"><c:out value="Connecticut"></c:out></option>
					<option value="Delaware"><c:out value="Delaware"></c:out></option>
					<option value="Florida"><c:out value="Florida"></c:out></option>
					<option value="Georgia"><c:out value="Georgia"></c:out></option>
					<option value="Hawaii"><c:out value="Hawaii"></c:out></option>
					<option value="Idaho"><c:out value="Idaho"></c:out></option>
					<option value="Illinois"><c:out value="Illinois"></c:out></option>
					<option value="Indiana"><c:out value="Indiana"></c:out></option>
					<option value="Iowa"><c:out value="Iowa"></c:out></option>
					<option value="Kansas"><c:out value="Kansas"></c:out></option>
					<option value="Kentucky"><c:out value="Kentucky"></c:out></option>
					<option value="Louisiana"><c:out value="Louisiana"></c:out></option>
					<option value="Maine"><c:out value="Maine"></c:out></option>
					<option value="Maryland"><c:out value="Maryland"></c:out></option>
					<option value="Massachusetts"><c:out value="Massachusetts"></c:out></option>
					<option value="Michigan"><c:out value="Michigan"></c:out></option>
					<option value="Minnesota"><c:out value="Minnesota"></c:out></option>
					<option value="Mississippi"><c:out value="Mississippi"></c:out></option>
					<option value="Missouri"><c:out value="Missouri"></c:out></option>
					<option value="Montana"><c:out value="Montana"></c:out></option>
					<option value="Nebraska"><c:out value="Nebraska"></c:out></option>
					<option value="Nevada"><c:out value="Nevada"></c:out></option>
					<option value="New Hampshire"><c:out value="New Hampshire"></c:out></option>
					<option value="New Jersey"><c:out value="New Jersey"></c:out></option>
					<option value="New Mexico"><c:out value="New Mexico"></c:out></option>
					<option value="New York"><c:out value="New York"></c:out></option>
					<option value="North Carolina"><c:out value="North Carolina"></c:out></option>
					<option value="North Dakota"><c:out value="North Dakota"></c:out></option>
					<option value="Ohio"><c:out value="Ohio"></c:out></option>
					<option value="Oklahoma"><c:out value="Oklahoma"></c:out></option>
					<option value="Oregon"><c:out value="Oregon"></c:out></option>
					<option value="Pennsylvania"><c:out value="Pennsylvania"></c:out></option>
					<option value="Rhode Island"><c:out value="Rhode Island"></c:out></option>
					<option value="South Carolina"><c:out value="South Carolina"></c:out></option>
					<option value="South Dakota"><c:out value="South Dakota"></c:out></option>
					<option value="Tennessee"><c:out value="Tennessee"></c:out></option>
					<option value="Texas"><c:out value="Texas"></c:out></option>
					<option value="Utah"><c:out value="Utah"></c:out></option>
					<option value="Vermont"><c:out value="Vermont"></c:out></option>
					<option value="Virginia"><c:out value="Virginia"></c:out></option>
					<option value="Washington"><c:out value="Washington"></c:out></option>
					<option value="West Virginia"><c:out value="West Virginia"></c:out></option>
					<option value="Wisconsin"><c:out value="Wisconsin"></c:out></option>
					<option value="Wyoming"><c:out value="Wyoming"></c:out></option>
					<option value="Other"><c:out value="I'm not from these parts"></c:out></option>
				</select>
			</div>
			<div class="surveyInputActivityLevel">
				<label for="activitylevel"><c:out value=" Activity Level: "></c:out></label> <select
					name="activitylevel" id="surveyActivitylevelDropdown">
					<option value="noneselected"><c:out value="---"></c:out></option>
					<option value="inactive"><c:out value="Inactive"></c:out></option>
					<option value="sedentary"><c:out value="Sedentary"></c:out></option>
					<option value="active"><c:out value="Active"></c:out></option>
					<option value="extremely active"><c:out value="Extremely Active"></c:out></option>
				</select>
			</div>

			<input class="surveyFormSubmitButton" type="submit"
				value="Submit Survey Post" />
		</form>

	</div>

</body>
</html>