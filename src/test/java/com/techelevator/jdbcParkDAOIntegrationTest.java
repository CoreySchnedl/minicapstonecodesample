package com.techelevator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.Models.Park;
import com.techelevator.npgeek.Models.jdbcParkDAO;

@Component
public class jdbcParkDAOIntegrationTest extends DAOIntegrationTest {
	
	jdbcParkDAO dao;
	
	@Before
	public void setup() {
		dao = new jdbcParkDAO(getDataSource());
	}

	@Test
	public void testGetAllParks() {
		List<Park> testParks = dao.getAllParks();
		Assert.assertTrue(testParks.size() == 10);
	}

	@Test
	public void checkToSeeThatMapRowToParkReturnsACompleteParkObject() {
		Park testCVNPpark = dao.getParkFromCode("CVNP");
		Assert.assertEquals("Cuyahoga Valley National Park", testCVNPpark.getParkname());
		Assert.assertEquals("CVNP", testCVNPpark.getParkcode());
		Assert.assertEquals("Ohio", testCVNPpark.getState());
		Assert.assertEquals(32832, testCVNPpark.getAcreage());
		Assert.assertEquals(696, testCVNPpark.getElevationinfeet());
		Assert.assertEquals(125.0, testCVNPpark.getMilesoftrail(), 0.01);
		Assert.assertEquals(0, testCVNPpark.getNumberofcampsites());
		Assert.assertEquals("Woodland", testCVNPpark.getClimate());
		Assert.assertEquals(2000, testCVNPpark.getYearfounded());
		Assert.assertEquals(2189849, testCVNPpark.getAnnualvisitorcount());
		Assert.assertEquals("Of all the paths you take in life, make sure a few of them are dirt.",
				testCVNPpark.getInspirationalquote());
		Assert.assertEquals("John Muir", testCVNPpark.getInspirationalquotesource());
		Assert.assertEquals(0, testCVNPpark.getEntryfee());
		Assert.assertEquals(390, testCVNPpark.getNumberofanimalspecies());

	}

}
