package com.techelevator;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.npgeek.Models.Park;
import com.techelevator.npgeek.Models.Survey;
import com.techelevator.npgeek.Models.jdbcParkDAO;
import com.techelevator.npgeek.Models.jdbcSurveyDAO;

public class jdbcSurveyDAOIntegrationTest extends DAOIntegrationTest {
	
	private jdbcSurveyDAO dao;
	private JdbcTemplate jdbcTemplate;
	
	
	@Before
	public void setup() {
		dao = new jdbcSurveyDAO(getDataSource());
	}

	@Test
	public void testSaveSurveyPost() {
		Survey testPost = new Survey();
		testPost.setActivitylevel("Active");
		testPost.setState("Ohio");
		testPost.setEmailaddress("bob@Test.com");
		testPost.setParkcode("CVNP");
		Survey returnedTestPost = dao.saveSurveyPost(testPost);
		Assert.assertEquals("Active", returnedTestPost.getActivitylevel());
		Assert.assertEquals("Ohio", returnedTestPost.getState());
		Assert.assertEquals("bob@Test.com", returnedTestPost.getEmailaddress());
		Assert.assertEquals("CVNP", returnedTestPost.getParkcode());
	}
	
	@Test
	public void testTalliedSurveyResults() {
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String clearSurveyTable = "Delete From survey_result";
		jdbcTemplate.update(clearSurveyTable);
		Survey testPost = new Survey();
		testPost.setActivitylevel("Active");
		testPost.setState("Ohio");
		testPost.setEmailaddress("bob@Test.com");
		testPost.setParkcode("ENP");
		Survey returnedTestPost = dao.saveSurveyPost(testPost);
		Assert.assertEquals("Everglades National Park", dao.tallySurveyResults());
	}

}
