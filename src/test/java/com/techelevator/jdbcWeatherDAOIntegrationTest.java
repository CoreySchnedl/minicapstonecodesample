package com.techelevator;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.npgeek.Models.Weather;
import com.techelevator.npgeek.Models.jdbcSurveyDAO;
import com.techelevator.npgeek.Models.jdbcWeatherDAO;

public class jdbcWeatherDAOIntegrationTest extends DAOIntegrationTest {
	
	private jdbcWeatherDAO dao;
	
	
	@Before
	public void setup() {
		dao = new jdbcWeatherDAO(getDataSource());
	}
	
	@Test
	public void getWeatherForecastReturnsCorrectListOfWeatherObjects() 
	{
		List<Weather> weatherList = dao.getWeatherForecast("MRNP");
		Assert.assertEquals(weatherList.get(0).getParkcode(), "MRNP");
		Assert.assertEquals(weatherList.get(1).getParkcode(), "MRNP");
		Assert.assertEquals(weatherList.get(2).getParkcode(), "MRNP");
		Assert.assertEquals(weatherList.get(3).getParkcode(), "MRNP");
		Assert.assertEquals(weatherList.get(4).getParkcode(), "MRNP");
		
		Assert.assertEquals(weatherList.get(0).getFivedayforecastvalue(), 1);
		Assert.assertEquals(weatherList.get(1).getFivedayforecastvalue(), 2);
		Assert.assertEquals(weatherList.get(2).getFivedayforecastvalue(), 3);
		Assert.assertEquals(weatherList.get(3).getFivedayforecastvalue(), 4);
		Assert.assertEquals(weatherList.get(4).getFivedayforecastvalue(), 5);
		
		Assert.assertEquals(weatherList.get(0).getHigh(), 30);
		Assert.assertEquals(weatherList.get(1).getHigh(), 32);
		Assert.assertEquals(weatherList.get(2).getHigh(), 27);
		Assert.assertEquals(weatherList.get(3).getHigh(), 27);
		Assert.assertEquals(weatherList.get(4).getHigh(), 25);
		
		Assert.assertEquals(weatherList.get(0).getLow(), 23);
		Assert.assertEquals(weatherList.get(1).getLow(), 24);
		Assert.assertEquals(weatherList.get(2).getLow(), 21);
		Assert.assertEquals(weatherList.get(3).getLow(), 23);
		Assert.assertEquals(weatherList.get(4).getLow(), 21);
		
		Assert.assertEquals(weatherList.get(0).getForecast(), "snow");
		Assert.assertEquals(weatherList.get(1).getForecast(), "snow");
		Assert.assertEquals(weatherList.get(2).getForecast(), "snow");
		Assert.assertEquals(weatherList.get(3).getForecast(), "snow");
		Assert.assertEquals(weatherList.get(4).getForecast(), "snow");
	}
	
	
	

}
