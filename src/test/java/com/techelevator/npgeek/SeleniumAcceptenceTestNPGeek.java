package com.techelevator.npgeek;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.npgeek.pageobjects.DetailParkPageObject;
import com.techelevator.npgeek.pageobjects.HomePageObject;


public class SeleniumAcceptenceTestNPGeek {
	
	private static WebDriver webDriver;
	private HomePageObject homePage;

	@BeforeClass
	public static void setup() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver= new ChromeDriver();
	}

	@Before
	public void goHome() {
		webDriver.get("http://localhost:8080/m3-java-capstone/");
		homePage = new HomePageObject(webDriver);
	}
	
	@AfterClass
	public static void closeBrowser() {
		webDriver.close();
	}
	
	@Test
	public void clickingOnImageTakesYouToCorrectDetailParkPage() {
		DetailParkPageObject cvnpDetail = homePage.clickParkImage("CVNP");
		Assert.assertEquals(cvnpDetail.getParkName(), "Cuyahoga Valley National Park");
		Assert.assertEquals(cvnpDetail.getState(), "Ohio");
		Assert.assertEquals(cvnpDetail.getYearFounded(), "Year founded: 2000");
		webDriver.get("http://localhost:8080/m3-java-capstone/");
		DetailParkPageObject evgDetail = homePage.clickParkImage("ENP");
		Assert.assertEquals(evgDetail.getParkName(), "Everglades National Park");
		Assert.assertEquals(evgDetail.getState(), "Florida");
		Assert.assertEquals(evgDetail.getYearFounded(), "Year founded: 1934");
	}
	
	@Test
	public void clickingOnTextTakesYouToCorrectDetailParkPage() {
		DetailParkPageObject cvnpDetail = homePage.clickParkText("CVNP");
		Assert.assertEquals(cvnpDetail.getParkName(), "Cuyahoga Valley National Park");
		Assert.assertEquals(cvnpDetail.getState(), "Ohio");
		Assert.assertEquals(cvnpDetail.getYearFounded(), "Year founded: 2000");
		webDriver.get("http://localhost:8080/m3-java-capstone/");
		DetailParkPageObject evgDetail = homePage.clickParkText("ENP");
		Assert.assertEquals(evgDetail.getParkName(), "Everglades National Park");
		Assert.assertEquals(evgDetail.getState(), "Florida");
		Assert.assertEquals(evgDetail.getYearFounded(), "Year founded: 1934");
	}
	
	@Test
	public void testCelsiusAndFahrenheitConversionFunctionality() {
		DetailParkPageObject cvnpDetail = homePage.clickParkText("CVNP");
		Assert.assertEquals(cvnpDetail.getHighForDay(1), "62 °F");
		Assert.assertEquals(cvnpDetail.getLowForDay(1), "38 °F");
		DetailParkPageObject cvnpDetailConvertedTemp = cvnpDetail.convertTempScale();
		Assert.assertEquals(cvnpDetailConvertedTemp.getHighForDay(1), "17 °C");
		Assert.assertEquals(cvnpDetailConvertedTemp.getLowForDay(1), "3 °C");
	}
	
}
