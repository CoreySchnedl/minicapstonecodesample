package com.techelevator.npgeek.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DetailParkPageObject {
	
	WebDriver webDriver;
	
	public DetailParkPageObject(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getParkName() {
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageParkTileInfo > div.detailPageParkTileName > span")).getText();
	}
	
	public String getState() {
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageParkTileInfo > div.detailPageParkTileLocation > span")).getText();
	}
	
	public String getYearFounded() {
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageParkTileInfo > div:nth-child(6) > div.detailPageParkTileYearFounded > span")).getText();
	}
	
	public String getHighForDay(int day){
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageWeatherContainer > div:nth-child("+(day+1)+") > div.weatherHighTemp > span")).getText();
	}
	
	public String getLowForDay(int day) {
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageWeatherContainer > div:nth-child("+(day+1)+") > div.weatherLowTemp > span")).getText();
	}
	
	public DetailParkPageObject convertTempScale() {
		WebElement submitButton = webDriver.findElement(By.cssSelector("#convertSubmitButton"));
		submitButton.submit();
		return new DetailParkPageObject(webDriver);
	}
	
	public String getForecastForDay(int day) {
		return webDriver.findElement(By.cssSelector("#detailPageMainBody > div > div.detailPageWeatherContainer > div:nth-child("+(day+1)+") > div.weatherForecast > span")).getText();
	}
	
	

}
