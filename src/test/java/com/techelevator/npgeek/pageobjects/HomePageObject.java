package com.techelevator.npgeek.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageObject {
	
	WebDriver webDriver;
	
	public HomePageObject(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public DetailParkPageObject clickParkImage(String parkCode) {
		int divNum = 1;
		if(parkCode.equals("CVNP")){
			divNum = 1;
		} else if (parkCode.equals("ENP")){
			divNum = 2;
		} else if (parkCode.equals("GCNP")){
			divNum = 3;
		} else if (parkCode.equals("GNP")){
			divNum = 4;
		} else if (parkCode.equals("GSMNP")){
			divNum = 5;
		} else if (parkCode.equals("GTNP")){
			divNum = 6;
		} else if (parkCode.equals("MRNP")){
			divNum = 7;
		} else if (parkCode.equals("RMNP")){
			divNum = 8;
		} else if (parkCode.equals("YNP")){
			divNum = 9;
		} else if (parkCode.equals("YNP2")){
			divNum = 10;
		} 
		WebElement parkImage = webDriver.findElement(By.cssSelector("#homePageMainBody > div:nth-child(" + divNum + ") > div.homePageParkTileImgContainer > a > img"));
		parkImage.click();
		return new DetailParkPageObject(webDriver);
	}
	
	public DetailParkPageObject clickParkText(String parkCode) {
		String stringToLookFor ="";
		if(parkCode.equals("CVNP")){
			stringToLookFor = "Cuyahoga Valley National Park";
		} else if (parkCode.equals("ENP")){
			stringToLookFor = "Everglades National Park";
		} else if (parkCode.equals("GCNP")){
			stringToLookFor ="Grand Canyon National Park";
		} else if (parkCode.equals("GNP")){
			stringToLookFor = "Glacier National Park";
		} else if (parkCode.equals("GSMNP")){
			stringToLookFor = "Great Smoky Mountains National Park";
		} else if (parkCode.equals("GTNP")){
			stringToLookFor = "Grand Teton National Park";
		} else if (parkCode.equals("MRNP")){
			stringToLookFor = "Mount Rainier National Park";
		} else if (parkCode.equals("RMNP")){
			stringToLookFor = "Rocky Mountain National Park";
		} else if (parkCode.equals("YNP")){
			stringToLookFor = "Yellowstone National Park";
		} else if (parkCode.equals("YNP2")){
			stringToLookFor = "Yosemite National Park";
		} 
		WebElement parkText = webDriver.findElement(By.linkText(stringToLookFor));
		parkText.click();
		return new DetailParkPageObject(webDriver);
	}
	
	public SurveyInputPageObject clickTakeSurveyLink() {
		WebElement takeSurveyLink = webDriver.findElement(By.linkText("Take our Survey"));
		takeSurveyLink.click();
		return new SurveyInputPageObject(webDriver);
	}
	
	
	
	
	
	

}