package com.techelevator.npgeek.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SurveyInputPageObject {

	WebDriver webDriver;
	
	public SurveyInputPageObject(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public void selectAPark(String parkFullName) {
		WebElement selectParkDropdown = webDriver.findElement(By.cssSelector("#surveyParkcodeDropdown"));
		Select dropdown = new Select(selectParkDropdown);
		dropdown.selectByVisibleText(parkFullName);
	}
	
	public void inputEmail(String email) {
		webDriver.findElement(By.name("emailaddress")).sendKeys(email);
	}
	
	public void selectAState(String state) {
		WebElement selectStateDropdown = webDriver.findElement(By.id("surveyStateDropdown"));
		Select dropdown = new Select(selectStateDropdown);
		dropdown.selectByVisibleText(state);
	}
	
	public void selectAnActivityLevel(String activityLevel) {
		WebElement selectActivityLevelDropdown = webDriver.findElement(By.id("surveyActivitylevelDropdown"));
		Select dropdown = new Select(selectActivityLevelDropdown);
		dropdown.selectByVisibleText(activityLevel);
	}
	
	public SurveyResultsPageObject submitSurvey() {
		WebElement submitButton = webDriver.findElement(By.cssSelector("body > div.surveyInputContainer > form > input"));
		submitButton.submit();
		return new SurveyResultsPageObject(webDriver);
	}
}
