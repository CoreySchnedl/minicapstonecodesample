package com.techelevator.npgeek.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SurveyResultsPageObject {
	
	WebDriver webDriver;
	
	public SurveyResultsPageObject(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getMostPopularPark() {
		return webDriver.findElement(By.id("surveyResult")).getText();
	}

}
